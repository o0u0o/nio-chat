package com.o0u0o.groupchat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/**
 * <h1>群聊系统服务器端</h1>
 * @author o0u0o
 * @date 2022/11/23 9:11 PM
 */
public class GroupChatServer {

    //选择器(也叫多路复用器)
    private Selector selector;

    //ServerSocketChannel 用于监听
    private ServerSocketChannel listenChannel;

    //监听端口
    private static  final int PORT  = 6667;

    //构造器
    //进行初始化工作
    public GroupChatServer(){
        try {
            //得到选择器
            selector = Selector.open();
            //初始化ServerSocketChannel
            listenChannel = ServerSocketChannel.open();
            //绑定端口
            listenChannel.socket().bind(new InetSocketAddress(PORT));
            //设置为非阻塞模式
            listenChannel.configureBlocking(false);
            //将该listenChannel 注册到Selector上
            listenChannel.register(selector, SelectionKey.OP_ACCEPT);
        }catch (IOException exception){
            exception.printStackTrace();
        }
    }

    //监听
    public void listen(){
        while (true){
            try {
                int count = selector.select(2000);
                //有事件处理
                if (count > 0) {
                    Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                    while(iterator.hasNext()){
                        //去除selectedKeys
                        SelectionKey key = iterator.next();

                        //监听到accept
                        if (key.isAcceptable()){
                            SocketChannel socketChannel = listenChannel.accept();
                            socketChannel.configureBlocking(false);
                            //将socketChannel注册到Selector上
                            socketChannel.register(selector, SelectionKey.OP_READ);
                            //提示xxx已经上线
                            System.out.println(socketChannel.getRemoteAddress() + "上线了");
                        }

                        //通道发送了Read(读)事件,即通道可读了
                        if (key.isReadable()){
                            //处理读（提取一个专门的方法）
                            readData(key);
                        }

                        //当前的key删除，防止重复处理
                        iterator.remove();
                    }
                } else{
                    System.out.println("等待...");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }

    //读取客户端消息
    private void readData(SelectionKey key){
        //定义一个SocketChannel
        SocketChannel channel = null;
        try {
            //取到关联的Channel
            channel = (SocketChannel) key.channel();
            //创建ByteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            //读取内容
            int read = channel.read(byteBuffer);
            //根据的read值处理
            if (read > 0) {
                //把缓冲区的数据转换成字符串
                String msg = new String(byteBuffer.array());
                //输出该消息
                System.out.println("来自客户端:" + msg.trim());

                //向其他的客户端转发消息,不包含自己(提取一个方法)
                sendMessage2OtherClient(msg, channel);

            }
        }catch (IOException exception){
            //对异常进行处理
            try {
                System.out.println(channel.getRemoteAddress() + "离线了");
                //取消注册
                key.cancel();
                //关闭通道
                channel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    //转换消息给其他客户端（也就是其他通道）
    private void sendMessage2OtherClient(String message, SocketChannel self) throws IOException {
        System.out.println("服务器转发消息中...");
        //遍历所有注册到 Selector 上的 SocketChannel
        for (SelectionKey  key : selector.keys()){
            //通过key取出对应的SocketChannel
            Channel targetChannel = key.channel();
            //排除自己
            if (targetChannel instanceof SocketChannel && targetChannel != self){
                //转发消息
                SocketChannel dest = (SocketChannel) targetChannel;
                //将message 存储到 buffer
                ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
                //将buffer 的数据 写入 到通道
                dest.write(buffer);
            }
        }
    }

    /**
     * 启动方法
     * @param args
     */
    public static void main(String[] args) {
        //创建服务器对象
        GroupChatServer groupChatServer = new GroupChatServer();
        groupChatServer.listen();
    }
}
