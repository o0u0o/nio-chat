package com.o0u0o.groupchat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * <h1>群聊系统客户端</h1>
 * @author o0u0o
 * @date 2022/11/23 9:54 PM
 */
public class GroupChatClient {
    //定义相关属性
    //服务器的IP
    private final String HOST = "127.0.0.1";

    //服务器端口
    private static  final int PORT  = 6667;

    //选择器(也叫多路复用器)
    private Selector selector;

    //SocketChannel 用于监听
    private SocketChannel socketChannel;

    //用户名
    private String username;

    //完成初始化工作
    public GroupChatClient(){
        try {
            selector = Selector.open();
            //连接服务器
            socketChannel = socketChannel.open(new InetSocketAddress(HOST, PORT));
            //设置 socketChannel 非阻塞
            socketChannel.configureBlocking(false);
            //将socketChannel注册到selector
            socketChannel.register(selector, SelectionKey.OP_READ);
            //得到username
            username = socketChannel.getLocalAddress().toString().substring(1);
            System.out.println(username + ": is ok...");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 向服务器发送消息
     * @param msg
     */
    public void  sendMsg(String msg){
        msg = username + "说：" + msg;
        try {
            socketChannel.write(ByteBuffer.wrap(msg.getBytes()));
        } catch (IOException exception){
            exception.printStackTrace();
        }
    }

    /**
     * 向服务器读取消息
     */
    public void readMsg(){
        try {

            //如果没有任务，2秒后做其他事情
            //int readChannels = selector.select(2000);
            int readChannels = selector.select();
            //有可以用的通道（有事件发生的通道）
            if (readChannels > 0){
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();

                while (iterator.hasNext()){
                    SelectionKey key = iterator.next();
                    if (key.isReadable()){
                        //得到相关通道
                        SocketChannel channel = (SocketChannel) key.channel();
                        //得到一个ByteBuffer
                        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                        //读取
                        channel.read(byteBuffer);
                        //把读到的缓冲区的数据转成字符串
                        String msg = new String(byteBuffer.array());
                        System.out.println(msg.trim());
                    }
                }
                //当前的SelectionKey删除，防止重复处理
                iterator.remove();
            } else {
                //System.out.println("没有可用的通道...");

            }
        } catch (IOException exception){
            exception.printStackTrace();
        }
    }

    /**
     * 启动方法
     * @param args
     */
    public static void main(String[] args) {
        //启动客户端
        GroupChatClient chatClient = new GroupChatClient();

        //启动一个线程 每隔3秒从服务器读取数据
        new Thread(){
            public void run(){
                //不停地去读取
                while (true){
                    chatClient.readMsg();
                    try {
                        //每隔3秒从服务器端读取
                        Thread.currentThread().sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        //发送数据给服务器端
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()){
            String msg = scanner.nextLine();
            chatClient.sendMsg(msg);
        }

    }


}
